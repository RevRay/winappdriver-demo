package ru.devray.winappdriver.demo;

import io.appium.java_client.windows.WindowsDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class NotepadTest {
    @Test
    public void testAddition() throws MalformedURLException {
//        WindowsOptions options = new WindowsOptions();
//        options.setApp("C:\\Windows\\System32\\notepad.exe");
//        options.setPlatformName(Platform.WINDOWS);
//        options.setPlatformName("Windows");
//        options.setCapability("app", "C:\\Windows\\System32\\notepad.exe");
//        options.setCapability("platform", "Windows");
//        options.setCapability("device", "WindowsPC");

                //probably a deprecated way - "You are sending "app" which is an invalid capability"
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("app", "C:\\Windows\\System32\\notepad.exe");
        caps.setCapability("platform", "Windows");
        caps.setCapability("device", "WindowsPC");

        WindowsDriver driver = new WindowsDriver(new URL("http://127.0.0.1:4723/"), caps);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.name("Help")).click();
        driver.findElement(By.name("About Notepad")).click();
        driver.findElement(By.name("OK")).click();
    }
}
